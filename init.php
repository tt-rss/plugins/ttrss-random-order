<?php
class Random_Order extends Plugin {

	function init($host) {
		$host->add_hook($host::HOOK_HEADLINES_CUSTOM_SORT_MAP, $this);
		$host->add_hook($host::HOOK_HEADLINES_CUSTOM_SORT_OVERRIDE, $this);
	}

	function hook_headlines_custom_sort_map() {
		return [
			"random_order" => "Random"
		];
	}

	function hook_headlines_custom_sort_override($order) {
		if ($order == "random_order") {
			return [ "RANDOM()", false ];
		} else {
			return [ "", false ];
		}
	}

	function about() {
		return array(null,
			"Return headlines in random order",
			"fox",
			false,
			"");
	}

	function api_version() {
		return 2;
	}

}
